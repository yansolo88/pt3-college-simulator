using System;
using UnityEngine;

public class Ecole : Building
{
	public int prix = 0;
	public int charges = 0;
	public int type = 0;
	public String name = "Ecole";
	public String description = "";
	public Texture textureMenu;

	public Ecole(){}

	public override int getPrix()
	{
		return this.prix;
	}

	public override int getCharges()
	{
		return this.charges;
	}

	public override int getType()
	{
		return this.type;
	}

	public override String getName()
	{
		return this.name;
	}

	public override String getDescription()
	{
		return this.description;
	}

	public override Texture getTexture()
	{
		return textureMenu;
	}
}

