using System;
using UnityEngine;

public class Residence : Building
{
	public int prix = 7500;
	public int charges = 0;
	public int type = 3;
	public String name = "Résidence étudiante";
	public String description = "Ceci est une description";
	public Texture textureMenu;

	public override int getPrix()
	{
		return this.prix;
	}
	
	public override int getCharges()
	{
		return this.charges;
	}

	public override int getType()
	{
		return this.type;
	}
	
	public override String getName()
	{
		return this.name;
	}

	public override String getDescription()
	{
		return this.description;
	}
	
	public override Texture getTexture()
	{
		return textureMenu;
	}
}

