using UnityEngine;
using System;

public class Building : ScriptableObject
{
	public virtual int getPrix(){return 0;}
	public virtual int getCharges(){return 0;}
	public virtual int getType(){return -1;}
	public virtual String getName(){return "";}
	public virtual String getDescription(){return "";}
	public virtual Texture getTexture(){return new Texture();}


	public Building fabriquer(int i)
	{
		switch(i)
		{
			case 0:
			{
				return new Ecole();
				break;
			}
			case 1:
			{
				return new Annexe();
				break;
			}
			case 2:
			{
				return new RestoU();
				break;
			}
			case 3:
			{
				return new Residence();
				break;
			}
		default:
			return this;
			break;
		}
	}
}

