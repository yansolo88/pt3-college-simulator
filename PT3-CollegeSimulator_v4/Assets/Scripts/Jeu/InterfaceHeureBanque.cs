﻿using UnityEngine;
using System.Collections;

public class InterfaceHeureBanque: MonoBehaviour {

    public int semaines;
    public int compteurMois;
    private string[] mois = new string[12]{ "Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"};
    public int année;
    public float timer;
    public Rect Heure;
    public GUISkin skinUIHeure;
    private int affichage=1;
    
    public void OnGUI()
    {
        GUI.skin = skinUIHeure;
        Heure = new Rect(Screen.width-180 , 30, 170, 100);
        GUI.Box(Heure, "");
        GUI.BeginGroup(Heure);
        if(GUI.Button(new Rect(0, 0, 85, 20), "Date"))
        {
            affichage = 1;
        }
        if(GUI.Button(new Rect(85, 0, 85, 20), "Budget"))
        {
            affichage = 2;
        }
        if (affichage == 1)
        {
            GUI.Label(new Rect(10, 20, 150, 30), "Semaine: " + semaines);
            GUI.Label(new Rect(10, 50, 150, 30), "Mois: " + mois[compteurMois]);
            GUI.Label(new Rect(10, 80, 150, 30), "Année: " + année);
        }
        if (affichage == 2)
        {
            GUI.Label(new Rect(10, 20, 150, 30), "Argent: " + semaines);
            GUI.Label(new Rect(10, 50, 150, 30), "Réputation: " + semaines);
            GUI.Label(new Rect(10, 80, 150, 30), "Nombre élèves: " + semaines);
        }
        GUI.EndGroup();
    }
    public void GestionTemps()
    {
        
            if (timer<0)
            {
                
                semaines++;
                timer = 15;

            }
            if (semaines > 4)
            {
                semaines = 1;
                compteurMois++;


            }
            if (compteurMois > 11)
            {
                compteurMois = 0;
                année++;

            }
            
        
    }



	// Use this for initialization
	void Start () {
        
        semaines = 1;
        compteurMois = 8;
        année = 2015;
        timer = 15;
    }

    // Update is called once per frame
    void Update()
    {
        
       
        GestionTemps();
        timer -= (Time.deltaTime);
    } 

}