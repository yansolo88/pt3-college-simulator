﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour
{
	public BoardManager boardScript;
	public Texture2D cursorTexture;
	//public Texture2D placeTexture;
	public CursorMode cursorMode = CursorMode.Auto;
	public Vector2 hotSpot = Vector2.zero;

	//private int level;

	// Use this for initialization
	void Awake()
	{
		Cursor.SetCursor(cursorTexture, hotSpot, cursorMode);

		boardScript = GetComponent<BoardManager>();
		InitGame();
	}

	void InitGame()
	{
		boardScript.SetupScene();
	}

	// Update is called once per frame
	void Update ()
	{

	}
}
