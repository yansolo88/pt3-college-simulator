﻿using UnityEngine;
using System.Collections;

public class InterfaceMenu : MonoBehaviour
{
	public GUISkin skinUIBarreAction;// skin de la barre menu
	public Texture[] imgMenu;
	public float facteurTemps=1;
	public int lB = 60; //largeur boutons
    public int hB = 40; //hauteur boutons
    public int nbB = 4;//nombre de boutons
    public int pBo = 19;//placement du groupe des boutons par la droite
    public int eB = 2;//espacement des boutons (optionnel suivant l'esthétique)
    public int numBm;//numéro boutton menu

	private InterfaceBatiments IB;
	private bool[] render=new bool[9];//tableau de booléen pour savoir quelle fenêtre est ouverte
	private int fenPausePosX = (Screen.width / 2) - 100; 
    private int fenPausePosY = 80;
    private int fenPauseL = 200;
    private int fenPauseH = 300;
    private int menuPosX = Screen.width / 2 - 300;
    private int menuPosY = Screen.height - 45;
    private int menuL= 600;
    private int menuH =40;
    private int menuAideX= Screen.width / 2 - 250;
    private int menuAideY= 80;
    private int menuAideL= 450;
    private int menuAIdeH= 450;
    


    public void OnGUI()
    {
       
        IB = GetComponent<InterfaceBatiments>();
        GUI.skin = skinUIBarreAction;//mise en place du skin de la barre de menu
        Rect barreBut = new Rect(menuPosX, menuPosY, menuL, menuH);
        Rect barreButClick = new Rect(menuPosX, menuPosY-586, menuL, menuH);
        Rect menuAide = new Rect(menuAideX, menuAideY, menuAideL, menuAIdeH);
        Rect menuPause = new Rect(fenPausePosX, fenPausePosY, fenPauseL, fenPauseH);
        GUI.Box(barreBut, "");
        GUI.BeginGroup(barreBut);

            int a = pBo;// l'initialisation d'une variable a semble obligatoire dans l'exécution à suivre
       
            for (int i = 0; i < nbB; i++)//création des boutons de la barre de menu
             {
            
                //ici le nom des bouttons sont "B.M"+i, sachant que les boutons seront certainement des images (une simple modification du script
                //le fera),il suffira simplement de créer un tableau d'images prenant en compte la variable i.

                if (GUI.Button(new Rect(a, 0, lB, hB), new GUIContent(imgMenu[i])))// si le boutton est cliqué met à jour le tableau d'ouverture des fenêtres
                {
                    numBm = i;
                    render[numBm] = !render[numBm];

                    if (IB.ouverture == true)
                    {
                        IB.ouverture = false;
                    }

            }
         
            
            a = a + lB + eB;

        }
        GUI.EndGroup();
        if (Input.GetKey(KeyCode.Escape))
          {
            setZero();
            IB.ouverture = false;
        }

     
       if (numBm == 0)
        {
            
            
            if (!((menuPause.Contains(Event.current.mousePosition))||(barreBut.Contains(Event.current.mousePosition))) && (Input.GetMouseButtonDown(0) || Input.GetMouseButtonDown(1)))
            {
                
                setZero();
            }
        }
        if(numBm==5)
        {
           
            if (!((menuAide.Contains(Event.current.mousePosition))||(barreBut.Contains(Event.current.mousePosition))) && (Input.GetMouseButtonDown(0) || Input.GetMouseButtonDown(1)))
            {
                setZero();
            }
        }
        actionBouton(numBm);//lors du clique d'un boutton, va exécuter l'action défini dans actionBouton.La fonction ne s'intègre apparament pas dans le if ci-dessus.
       
    }

    //La fonction actionBouton pourrait éventuellement être optimisée afin de gérer le numéro de case des render
    //automatiquement tout comme les paramètres des fonctions réinitialisationRender

    private void actionBouton(int a)//définition des actions des boutons
    {

        switch (a)
        {
            case 0:
                if (render[0] == true)
                {
                    réinitialisationRender(0);//voir fonction réinitialisationRender
                    //création fenêtre
                    GUI.Window(0, new Rect(fenPausePosX, fenPausePosY, fenPauseL, fenPauseH), MenuPause, "Menu Pause");
                    Time.timeScale = 0;//pause
                }
                else
                {
                    Time.timeScale = 1;//reprise
                }
                break;
            case 1:
                Time.timeScale = 1;
                break;
            case 2:

                Time.timeScale = 4;
                break;
            case 3:
               //sauvegarde rapide
                break;
            case 4:
                //exédution événement aléa
                break;
            case 5:
                
                if (render[5] == true)
                {
                    réinitialisationRender(5);
                    GUI.Window(5, new Rect(Screen.width / 2 - 250, 80, 450, 300),Aide, "Menu aide");
                }
                break;
           
        }
    }


    private void réinitialisationRender(int a)//lorsque qu'une fenêtre est ouverte, ferme toutes les autres pour une meilleur naviguation
    {
        for (int i = 0; i < nbB; i++)
        {
            render[i] = false;
        }
        render[a] = true;
    }

    public void setZero()
    {
        for (int i = 0; i < nbB; i++)
        {
            render[i] = false;
        }
    }

    public bool isNull()
    {
        for(int i=0; i < render.Length; i++)
        {
            if (render[i] != false)
            {
                return false;
            }
        }
        return true;
    }


    private void MenuPause(int id) //contenu de la fenêtre MenuPause
    {
        GUI.Label(new Rect(90, 30, 100, 30), System.DateTime.Now.ToString("hh:mm"));
        if (GUI.Button(new Rect(10, 70, 180, 30), "Save"))
        {

        }
        if(GUI.Button(new Rect(10, 110, 180, 30), "Exit"))
        {
            Application.Quit();
        }
        if (GUI.Button(new Rect(10, 150, 180, 30), "Options"))
        {
			
		}
    }


    private void Aide(int id)//contenu de la fenêtre MenuTest
    {
        string texte = "écrire l'aide";
        GUI.Label(new Rect(20, 20, 460, 260), texte);
    }

    // Use this for initialization
    void Start () {
         nbB = 6;//nombre de boutons
    fenPausePosX = (Screen.width / 2) - 100;
    fenPausePosY = 80;
    fenPauseL = 200;
    fenPauseH = 300;
    menuPosX = Screen.width / 2 - 175;
    menuPosY = Screen.height - 45;
    menuL = 400;
    menuH = 40;
}
	
	// Update is called once per frame
	void Update () {
        
      

    }
}
