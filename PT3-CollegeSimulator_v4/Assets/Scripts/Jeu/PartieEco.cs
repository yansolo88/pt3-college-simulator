﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using System.Collections;



class PartieEco : MonoBehaviour
{
    public float compte = 50000;
    private PlacementManager placementManager;
    private int[] nbrBat = {0,0,0,0};
    public InterfaceHeureBanque IH;
    private Etudiants etudiant; // etudiant = objet de Etudiant
    private int eleves;

    public static float repas = 3.5F;
    public static int prixScolarité = 150;
    public static int prêt = 30000;
    public static int alerte = 20000;
    public static int rendruParMois = 3250;
    public static int prixEcole = 25000;
    public static int prixAnnexe = 10000;
    public static int prixRU = 20000;
    public static int prixResidence = 15000;
    public int compteurFin = 0;

    void Start()
    {
        placementManager = GetComponent<PlacementManager>();
        IH = GetComponent<InterfaceHeureBanque>();
        etudiant = GetComponent<Etudiants>();
        eleves = etudiant.eleve;
    }

    void Update()
    {

            CompteQuiAugmente();
            PrixBatiment();
            Charges();
            CompteFinDePartie();
        
    }



    private void PrixBatiment()
    {
        if (placementManager.buildingsPlaced[3] > nbrBat[3])
        {
            compte = compte - prixResidence;
            nbrBat[3] = placementManager.buildingsPlaced[3];
        }

        if (placementManager.buildingsPlaced[2] > nbrBat[2])
        {
            compte = compte - prixRU;
            nbrBat[2] = placementManager.buildingsPlaced[2];
        }

        if (placementManager.buildingsPlaced[1] > nbrBat[1])
        {
            compte = compte - prixAnnexe;
            nbrBat[1] = placementManager.buildingsPlaced[1];
        }

        if (placementManager.buildingsPlaced[0] > nbrBat[0])
        {
            compte = compte - prixEcole;
            nbrBat[0] = placementManager.buildingsPlaced[0];
        }
    }


    private void CompteQuiAugmente()
    {

        int compteur = 0;
        while (compteur != 4)
        {
            if (IH.timer == 0)
            {
                compteur++;
                if (1 <= compteur && compteur <= 4)
                {
                    compte = compte + eleves*repas; // prend en fonction le nombre d'étudiant fois le prix du repas
                }
            }
        }
        compte = compte + eleves* prixScolarité; // prend en fonction le nombre d'étudiant fois le prix par mois de la scolarité
        compteur = 0;

    }




    private void CompteFinDePartie()
    {
        if (compte <= - alerte)
        {
            compteurFin = compteurFin + 1;
            compte = compte + prêt;
            if (compte <= -alerte && compteurFin == 1)
            {
               // GUI.Window(0, new Rect(0, 0, 100, 100), Fenetre, " Construire un Batiment Ecole pour commencer");
            }
            if (compte >= 0)
            {
                compte = compte - rendruParMois;
            }
        }
    }


    private void Fenetre(int id)
    {
        GUI.Button(new Rect(10, 40, 80, 30), "OK");
    }

    private void Charges()
    {
        int compteur = 0;
        while (compteur != 4)
        {
            if (IH.timer == 0)
            {
                compteur++;
            }
        }
        compte = compte - 3000 * nbrBat[0];
        compte = compte - 3000 * nbrBat[1];
        compte = compte - 3000 * nbrBat[2];
        compte = compte - 3000 * nbrBat[3];
        compteur = 0;
    }



 /*   public void imprevus()
    {

        if (evenement.eau == true)
        {
            compte = compte - 5000 * (nbrBat[0]+ nbrBat[1]+ nbrBat[2]+ nbrBat[3]); // s'applique à toute la clesse batiment
        }
    }*/
}

