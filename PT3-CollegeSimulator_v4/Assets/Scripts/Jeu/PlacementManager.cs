using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlacementManager : MonoBehaviour {

	public BoardManager boardManager;
	public GameObject roadGO;
	public GameObject destroyGO;
	public int[] buildingsPlaced = new int[5]; // tableau de nombres de buildings placés par catégorie
	public Texture2D destroyCursor;
	public Texture2D defaultCursor;
	public Texture2D placeCursor;
	public Sprite okText;
	public Sprite nopeText;
	public Sprite defaultText;

	private GameObject[,] board;
	private Transform currentBuilding;
	private Transform buildingsHolder;
	private bool hasPlaced;
	private int buildingsType, TileX, TileY, RegionDX, RegionDY, columns, rows;
	private GameObject i; // building à placer
	//private ArrayList buildingsList;
	private List<Building> buildingsList = new List<Building>();

	// Use this for initialization
	void Start()
	{
		buildingsHolder = new GameObject("Buildings").transform;
		board = boardManager.board;
		columns = boardManager.columns;
		rows = boardManager.rows;
	}

	float sign(Vector2 v1, Vector2 v2, Vector2 v3)
	{
		return (v1.x - v3.x) * (v2.y - v3.y) - (v2.x - v3.x) * (v1.y - v3.y);
	}

	bool pointInTriangle(Vector2 pt, Vector2 v1, Vector2 v2, Vector2 v3)
	{
		bool b1, b2, b3;
		b1 = sign (pt, v1, v2) < 0.0f;
		b2 = sign (pt, v2, v3) < 0.0f;
		b3 = sign (pt, v3, v1) < 0.0f;
		return((b1 == b2) && (b2 == b3));
	}

	// Update is called once per frame
	void Update ()
	{
		Camera camera = GetComponent<Camera>();
		if(currentBuilding != null && !hasPlaced)
		{
		//http://www.gamedev.net/page/resources/_/technical/game-programming/isometric-n-hexagonal-maps-part-i-r747
				
			Vector3 m = Input.mousePosition;
			m = new Vector3(m.x, m.y, transform.position.z);
			Vector3 p = camera.ScreenToWorldPoint(m);

			if(p.x<0)
				p.x = 0;
			if(p.x>columns*1.3f)
				p.x = columns*1.3f;

			if(p.y<0)
				p.y = 0;
			if(p.y>rows*0.65f)
				p.y = rows*0.65f;

			int RegionX = Mathf.FloorToInt(p.x / 1.3f);
			int RegionY = Mathf.FloorToInt(p.y / 0.65f) * 2;

			float MouseMapX = p.x % 1.3f;
			float MouseMapY = p.y % 0.65f;

			Vector2 MouseMapPt = new Vector2(MouseMapX, MouseMapY);
			if(pointInTriangle(MouseMapPt, new Vector2(0.0f, 0.0f), new Vector2(0.65f, 0.0f), new Vector2(0.0f, 0.325f)))
			{
				//Vert
				RegionDX = -1;
				RegionDY = -1;
			}
			else
			{
				if(pointInTriangle(MouseMapPt, new Vector2(0.65f, 0.0f), new Vector2(1.3f, 0.0f), new Vector2(1.3f, 0.325f)))
				{
					//Bleu
					RegionDX = 0;
					RegionDY = -1;
				}
				else
				{
					if(pointInTriangle(MouseMapPt, new Vector2(1.3f, 0.325f), new Vector2(1.3f, 0.65f), new Vector2(0.65f, 0.65f)))
					{
						//Jaune
						RegionDX = 0;
						RegionDY = 1;
					}
					else
					{
						if(pointInTriangle(MouseMapPt, new Vector2(0.65f, 0.65f), new Vector2(0.0f, 0.65f), new Vector2(0.0f, 0.325f)))
						{
							//Rouge
							RegionDX = -1;
							RegionDY = 1;
						}
						else
						{
							//Blanc
							RegionDX = 0;
							RegionDY = 0;
						}
					}
				}
			}

			TileX = RegionX + RegionDX;
			TileY = RegionY + RegionDY;

			if(TileX < 0)
				TileX = 0;
			if(TileY < 0)
				TileY = 0;
			if(TileX >= columns)
				TileX = columns-1;
			if(TileY >= rows)
				TileY = rows-1;

			if(TileY % 2 == 0)
			{
				TileY++;
			}
			else
			{
				TileX++;
				TileY++;
			}

			float tx = board[TileX,TileY].transform.position.x;
			float ty = board[TileX,TileY].transform.position.y;

			//placer un building

			if(i.tag.Equals("Building"))
			{
				Cursor.SetCursor(placeCursor, Vector2.zero, CursorMode.Auto);
				currentBuilding.position = new Vector3(tx, ty+0.3f, 0);

				if(Input.GetMouseButtonDown(0))
				{
					currentBuilding.position = new Vector3(tx, ty, 0);

					//si la place est disponible
					if(isLegalPosition())
					{
						Cursor.SetCursor(defaultCursor, Vector2.zero, CursorMode.Auto);
						hasPlaced = true;

						Building builder = new Building(); //fabrique concrète

						Building build = builder.fabriquer(buildingsType);	// produit de la fabrique
						//print(build.GetType());

						buildingsList.Add(build);

						//print(build.getName());
						//print(build.getDescription());

						i.GetComponent<SpriteRenderer>().color += new Color(0f, 0f, 0f, 0.3f);
						DestroyObject(board[TileX, TileY]);
						board[TileX, TileY] = i;
						i.transform.SetParent(buildingsHolder);
						buildingsPlaced[buildingsType]++;
						boardManager.refresh();

						if(i.name.Equals("Ground_4(Clone)"))
							setItems(roadGO, 4);
					}
					else
						print("Access Denied !");
				}
			}

			if(i.tag.Equals("Terrain"))
			{
				Cursor.SetCursor(destroyCursor, Vector2.zero, CursorMode.Auto);
				currentBuilding.position = new Vector3(tx, ty+0.15f, 0);

				if(isLegalPositionDestroy())
					i.GetComponent<SpriteRenderer>().sprite = okText;
				else
					i.GetComponent<SpriteRenderer>().sprite = nopeText;

				if(Input.GetMouseButtonDown(0))
				{
					currentBuilding.position = new Vector3(tx, ty, 0);

					if(isLegalPositionDestroy())
					{
						i.GetComponent<SpriteRenderer>().sprite = defaultText;
						Cursor.SetCursor(defaultCursor, Vector2.zero, CursorMode.Auto);
						hasPlaced = true;
						i.GetComponent<SpriteRenderer>().color += new Color(0f, 0f, 0f, 0.3f);
						//int buildingsType = board[TileX, TileY].getType();
						//buildingsPlaced[buildingsType]--;
						DestroyObject(board[TileX, TileY]);
						board[TileX, TileY] = i;
						i.transform.SetParent(boardManager.boardHolder);
						boardManager.refresh();

						setItems(destroyGO, 5);
					}
					else
					{
						Cursor.SetCursor(defaultCursor, Vector2.zero, CursorMode.Auto);
						print("Access Denied !");
					}
				}
			}

			if(Input.GetKey(KeyCode.Escape))
			{
				DestroyObject(i);
				Cursor.SetCursor(defaultCursor, Vector2.zero, CursorMode.Auto);
			}
		}
	}

	bool isLegalPosition()
	{
		return board[TileX, TileY].tag.Equals("Terrain");
	}

	bool isLegalPositionDestroy()
	{
		return board[TileX, TileY].tag.Equals("Building");
	}

	public void setItems(GameObject g, int type)
	{
		hasPlaced = false;
		buildingsType = type;
		i = Instantiate(g) as GameObject;
		currentBuilding = i.transform;
		i.GetComponent<SpriteRenderer>().sortingOrder = 1000000;
		i.GetComponent<SpriteRenderer>().color += new Color(0f, 0f, 0f, -0.3f);
	}

	public List<Building> getBuildingList()
	{
		return buildingsList;
	}
}
