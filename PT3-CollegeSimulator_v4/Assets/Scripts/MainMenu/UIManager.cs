﻿using UnityEngine;
using System.Collections;

public class UIManager : MonoBehaviour {

	public void DisableBoolInAnimator(Animator anim)
	{
		anim.SetBool("isDisplayed", false);
	}
	
	public void EnableBoolInAnimator(Animator anim)
	{
		anim.SetBool("isDisplayed", true);
	}
	
	public void NavigateTo(string scene)
	{
		Application.LoadLevel(scene);
	}

	public void StopAllCoroutines()
	{
		Application.Quit ();
	}
}
