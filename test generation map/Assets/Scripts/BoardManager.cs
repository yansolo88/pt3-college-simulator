﻿using UnityEngine;
using System;
using System.Collections.Generic;
using Random = UnityEngine.Random;

public class BoardManager : MonoBehaviour
{
	public int columns = 16;
	public int rows = 10;
	public GameObject[] groundTiles;

	private Transform boardHolder;
	private List <Vector3> gridPositions = new List<Vector3>();

	void InitialiseList()
	{
		gridPositions.Clear();
	}

	void BoardSetup()
	{
		boardHolder = new GameObject("Board").transform;

		GameObject toInstantiate = groundTiles[0];

		for(int x = 0; x < columns + 1; x++)
		{			
			for(int y = 0; y < rows + 1; y++)
			{
				//GameObject toInstantiate = groundTiles[Random.Range (0,groundTiles.Length)];
				
				GameObject instance = Instantiate (toInstantiate, 
				                                   new Vector3 (1.3f*x, (y - 0.35f*y), 0f), 
				                                   Quaternion.identity) as GameObject;

				instance.transform.SetParent (boardHolder);

				GameObject instance2 = Instantiate (toInstantiate, 
				                                    new Vector3 ((0.65f + 1.3f*x), (0.325f + 0.65f*y), 0f), 
				                                    Quaternion.identity) as GameObject;

				instance2.transform.SetParent (boardHolder);
			}
		}

		/*
		for(int x = columns + 1; x > 0; x--)
		{			
			for(int y = rows + 1; y > 0; y--)
			{
				//GameObject toInstantiate = groundTiles[Random.Range (s0,groundTiles.Length)];
				
				GameObject instance = Instantiate (toInstantiate, 
				                                   new Vector3 ((0.65f + 1.3f*x), (0.325f + 0.65f*y), 0f), 
				                                   Quaternion.identity) as GameObject;
				
				instance.transform.SetParent (boardHolder);
				
				GameObject instance2 = Instantiate (toInstantiate, 
				                                    new Vector3 (1.3f*x, (y - 0.35f*y), 0f), 
				                                    Quaternion.identity) as GameObject;
				
				instance2.transform.SetParent (boardHolder);
				
			}
		}
		*/
	}

	public void SetupScene()
	{
		BoardSetup();

	}
}
