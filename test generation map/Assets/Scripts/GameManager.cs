﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour
{
	public BoardManager boardScript;

	//private int level;

	// Use this for initialization
	void Awake()
	{
		boardScript = GetComponent<BoardManager>();
		InitGame();
	}

	void InitGame()
	{
		boardScript.SetupScene();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
