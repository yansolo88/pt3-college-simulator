﻿using UnityEngine;
using System.Collections;

public class Internet_Greve : Evenement 
{
	private string nom = "Panne Internet / Grève";
	private int coutReparations = 0;
	private int malus = 30;

	public override string getNom()
	{
		return nom;
	}

	public override int getCout()
	{
		return coutReparations;
	}

	public override int getMalus()
	{
		return malus;
	}
}
