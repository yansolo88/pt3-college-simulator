using System;
using UnityEngine;

public class RestoU : Building
{
	public int prix = 7500;
	public int charges = 1500;
	public int type = 2;
	public String name = "Resto Universitaire";
	public String description = "Empêcher vos étudiants de s'évanouir sur leurs travaux en les nourrissant convenablement.";

	public RestoU(int x, int y)
	{
		this.x = x;
		this.y = y;
	}

	public override int getPrix()
	{
		return this.prix;
	}
	
	public override int getCharges()
	{
		return this.charges;
	}

	public override int getType()
	{
		return this.type;
	}
	
	public override String getName()
	{
		return this.name;
	}
	
	public override String getDescription()
	{
		return this.description;
	}
}

