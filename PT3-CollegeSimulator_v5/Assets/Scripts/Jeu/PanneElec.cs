﻿using UnityEngine;
using System.Collections;

public class PanneElec : Evenement 
{
	private string nom = "Panne d'électricité";
	private int coutReparations = 0;
	private int malus = 35;

	public override string getNom ()
	{
		return nom;
	}

	public override int getCout()
	{
		return coutReparations;
	}

	public override int getMalus()
	{
		return malus;
	}
}
