﻿using UnityEngine;
using System.Collections;

public class CameraManager : MonoBehaviour {

	public BoardManager board;
	public float CameraMoveSpeed;

	private int zoneMorte = 10;

	// Gestion du zoom par la molette de la souris
	void ZoomScroll()
	{
		if(Time.timeScale != 0)
		{
			var varScroll = Input.GetAxis("Mouse ScrollWheel");
			if(varScroll < 0f || Input.GetKeyDown(KeyCode.F))
			{
				if(Camera.main.orthographicSize < 10)
				{
					Camera.main.orthographicSize += 0.5f;
				}
			}
			if(varScroll > 0f || Input.GetKeyDown(KeyCode.R))
			{
				if(Camera.main.orthographicSize > 4)
				{
					Camera.main.orthographicSize -= 0.5f;
				}
			}
		}
	}

	// Gestion du déplacement de la caméra par les touches directionnelles et la souris en bord d'écran
	void CameraMove()
	{
		if(((Input.GetKey(KeyCode.UpArrow) || Input.mousePosition.y > Camera.main.pixelHeight - zoneMorte)) && transform.position.y < ((board.rows/2)*0.65f)-3)
		{
			transform.Translate(new Vector3(0,CameraMoveSpeed * Time.deltaTime,0));
		}
		if(((Input.GetKey(KeyCode.DownArrow) || Input.mousePosition.y < zoneMorte)) && transform.position.y > 3)
		{
			transform.Translate(new Vector3(0,-CameraMoveSpeed * Time.deltaTime,0));
		}
		if(((Input.GetKey(KeyCode.RightArrow) || Input.mousePosition.x > Camera.main.pixelWidth - zoneMorte)) && transform.position.x < (board.columns*1.3f)-3)
		{
			transform.Translate(new Vector3(CameraMoveSpeed * Time.deltaTime,0,0));
		}
		if(((Input.GetKey(KeyCode.LeftArrow) || Input.mousePosition.x < zoneMorte)) && transform.position.x > 3.5)
		{
			transform.Translate(new Vector3(-CameraMoveSpeed * Time.deltaTime,0,0));
		}
	}

	// Update is called once per frame
	void Update ()
	{
		ZoomScroll();
		CameraMove();
	}
}
