﻿using System;
using UnityEngine;

public class Route : Building
{
    public int prix = 0;
    public int charges = 0;
    public int type = 4;
    public String name = "Route";
    public String description = "Ces routes sont seulement là pour pouvoir mettre un peu de réalisme... Ne servent à rien !";

    public Route(int x, int y)
    {
        this.x = x;
        this.y = y;
    }

    public override int getPrix()
    {
        return this.prix;
    }

    public override int getCharges()
    {
        return this.charges;
    }

    public override int getType()
    {
        return this.type;
    }

    public override String getName()
    {
        return this.name;
    }

    public override String getDescription()
    {
        return this.description;
    }
}

