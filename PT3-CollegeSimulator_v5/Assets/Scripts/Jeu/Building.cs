using UnityEngine;
using System;

public class Building : ScriptableObject
{
	public int x;
	public int y;
	public virtual int getPrix(){return 0;}
	public virtual int getCharges(){return 0;}
	public virtual int getType(){return -1;}
	public virtual String getName(){return "";}
	public virtual String getDescription(){return "";}

	public Building fabriquer(int i, int x, int y)
	{
		switch(i)
		{
			case 0:
			{
				return new Ecole(x, y);
				break;
			}
			case 1:
			{
				return new Annexe(x, y);
				break;
			}
			case 2:
			{
				return new RestoU(x, y);
				break;
			}
			case 3:
			{
				return new Residence(x, y);
				break;
			}
            case 4:
            {
                return new Route(x, y);
                break;
            }
            default:
			return this;
			break;
		}
	}
}

