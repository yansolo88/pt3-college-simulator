using System;
using UnityEngine;

public class Residence : Building
{
	public int prix = 12500;
	public int charges = 300;
	public int type = 3;
	public String name = "Résidence étudiante";
	public String description = "Vous ne formez pas des SDF, si ? Logez-les, ils vous en seront reconnaissants !";

	public Residence(int x, int y)
	{
		this.x = x;
		this.y = y;
	}

	public override int getPrix()
	{
		return this.prix;
	}
	
	public override int getCharges()
	{
		return this.charges;
	}

	public override int getType()
	{
		return this.type;
	}
	
	public override String getName()
	{
		return this.name;
	}

	public override String getDescription()
	{
		return this.description;
	}
}

