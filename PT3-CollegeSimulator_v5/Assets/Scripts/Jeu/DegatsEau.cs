﻿using UnityEngine;
using System.Collections;

public class DegatsEau : Evenement 
{
	private string nom = "Innondation";
	private int coutReparations = 15000;
	private int malus = 20;

	public override string getNom()
	{
		return nom;
	}

	public override int getCout()
	{
		return coutReparations;
	}

	public override int getMalus()
	{
		return malus;
	}
}
