﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using System.Collections;



public class Etudiants : MonoBehaviour
{
    public PlacementManager placementManager;
    public InterfaceHeureBanque IH;
	public InterfaceMenu IM;
	public PartieEco PE;
	public GUISkin skinUI;
	public Texture textAlerte;
	public int eleve;
	public int reputation;

	private bool depart;
	private bool fenDepBool;
	private bool fenResBool;
	private bool fenRuBool;
	private bool fenEABool;
	private bool fenEventBool;
	private int[] nbrBat = {0,0,0,0};
	private int totalEleve;
    private int eleveRUCantine;
    private int capaciteRU;
    private int capaciteResidence;
    private int capaciteARU;
    private int capaciteER;
    private int random;
	private int semaines;
	private int mois;
	private int annee;
	private int eventCout;
	private int eventMalus;
	private string eventNom;

    void Start()
    {
		eleve = 0;
        depart = true;
		fenDepBool = false;
		fenResBool = false;
		fenRuBool = false;
		fenEABool = false;

		fenEventBool = false;
		eventCout = 0;
		eventMalus = 0;
		eventNom = "";

		reputation = 110;
		totalEleve = 0;

		capaciteRU = 0;
		capaciteResidence = 0;
		capaciteARU = 250;
		capaciteER = 500;

		this.semaines = IH.semaines;
		this.mois = IH.compteurMois;
		this.annee = IH.annee;
    }



     private void constructEcole()
     {
         if (nbrBat[0] == 0)
         {
             fenDepBool = true ;         
         }
     }


    private void eleveDepart()
    {

        if (nbrBat[0] == 1 && depart)
        {
            random = Random.Range(1, 500);
            eleve = random;
            depart = false;
			fenDepBool = false;
        }
    }


	private void gestionCapaciteBat()
    {
		if (nbrBat[0] >= 1)
		{
			if (eleve <= totalEleve )
			{
				fenEABool = false;

				if (eleve <= capaciteRU)
				{
					fenRuBool = false;

					if (eleve <= capaciteResidence)
					{
						fenResBool = false;

						if(IH.compteurMois > this.mois)
						{
							random = Random.Range(1, 50);;
							eleve += random;
                            reputation += 20;
						}
					}
					else
					{
						fenResBool = true;
						
						if(IH.semaines > this.semaines)
						{
							reputation = reputation - 5;
							this.semaines = IH.semaines;
						}
					}
					this.mois = IH.compteurMois;
				}
				else
				{
					fenRuBool = true;
						
					if(IH.semaines > this.semaines)
					{
						reputation = reputation - 5;
						this.semaines = IH.semaines;
					}
				}
			}
			else
			{
				fenEABool = true;
			}
		}
	}
	
	
	
	
	private void augmentationCapaciteBat()
	{ 
		// Ajout de batiments
		if (placementManager.buildingsPlaced[3] > nbrBat[3])
		{
			capaciteResidence += capaciteER;
            nbrBat[3] = placementManager.buildingsPlaced[3];
        }

        if (placementManager.buildingsPlaced[2] > nbrBat[2])
        {
            capaciteRU += capaciteARU;
            nbrBat[2] = placementManager.buildingsPlaced[2];
        }

        if (placementManager.buildingsPlaced[1] > nbrBat[1])
        {
            totalEleve += capaciteARU;
            nbrBat[1] = placementManager.buildingsPlaced[1];
        }

        if (placementManager.buildingsPlaced[0] > nbrBat[0])
        {
            totalEleve += capaciteER;
            nbrBat[0] = placementManager.buildingsPlaced[0];
        }

		// Suppression de batiments
		if (placementManager.buildingsPlaced[3] < nbrBat[3])
		{
			capaciteResidence -= capaciteER;
			nbrBat[3] = placementManager.buildingsPlaced[3];
		}
		
		if (placementManager.buildingsPlaced[2] < nbrBat[2])
		{
			capaciteRU -= capaciteARU;
			nbrBat[2] = placementManager.buildingsPlaced[2];
		}
		
		if (placementManager.buildingsPlaced[1] < nbrBat[1])
		{
			totalEleve -= capaciteARU;
			nbrBat[1] = placementManager.buildingsPlaced[1];
		}
		
		if (placementManager.buildingsPlaced[0] < nbrBat[0])
		{
			totalEleve -= capaciteER;
			nbrBat[0] = placementManager.buildingsPlaced[0];
		}
    }
	

    private void elevesSortants()
    {
        if (IH.annee > this.annee)
        {
            random = Random.Range(1, eleve);
            eleve = eleve - random;
			this.annee = IH.annee;
        }
    }

	public IEnumerator wait10sec()
	{
		yield return new WaitForSeconds(10);
		fenEventBool = false;
	}

	public void imprevus()
	{
		if(IM.ev != null)
		{
			PE.compte -= IM.ev.getCout();
			reputation -= IM.ev.getMalus();

			eventCout = IM.ev.getCout();
			eventMalus = IM.ev.getMalus();
			eventNom = IM.ev.getNom();
			fenEventBool = true;

			IM.ev = null;

			StartCoroutine(wait10sec());
		}
	}

    void OnGUI()
    {
		GUI.skin = skinUI;
		Rect alerteBox = new Rect(Screen.width-380 , 30, 200, 100);
		GUI.Box(alerteBox, "");
		GUI.BeginGroup(alerteBox);

		GUI.Label(new Rect(10, 0, 100, 40), new GUIContent("Alerte", textAlerte));
		GUI.Label(new Rect(10, 30, 180, 40), new GUIContent("----------------------"));

		if(fenDepBool)
			GUI.Label(new Rect(10, 50, 180, 70), "Veuillez construire une École pour commencer !");

		if(fenResBool)
			GUI.Label(new Rect(10, 50, 180, 70), "Veuillez construire une Résidence !");

		if(fenRuBool)
			GUI.Label(new Rect(10, 50, 180, 70), "Veuillez construire un Restaurant !");

		if(fenEABool)
			GUI.Label(new Rect(10, 50, 180, 70), "Veuillez construire une Ecole ou une annexe !");

		if(fenEventBool)
		{
			GUI.Label(new Rect(10, 50, 180, 70), "Une " + eventNom + " viens de vous faire perdre " + eventCout + "€ et " + eventMalus + " de réputation.");
			//new WaitForSeconds(10);
			//StartCoroutine(wait10sec());
			//fenEventBool = false;
		}

		GUI.EndGroup();
    }

	void Update()
	{
		constructEcole();
		eleveDepart();
		augmentationCapaciteBat();
		gestionCapaciteBat();
		elevesSortants();
		imprevus();
	}
}
