﻿using UnityEngine; 
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class EventManager : MonoBehaviour
{
	public PlacementManager pm;
	private List<Building> buildingsList;

	private Building selectedBuilding;

	void Start()
	{
		buildingsList = pm.getBuildingList();
	}

	public bool isPossible()
	{
		return (buildingsList.Count == 0);
	}

	public void selectBuilding()
	{
		if(!isPossible())
		{
			print("Event impossible");
		}
		else
		{
			int i = (int)Mathf.Floor(Random.Range(0.0f, buildingsList.Count));
			selectedBuilding = buildingsList.ElementAt(i);
		}
	}

	public Evenement selectEvent()
	{
		int selectedEvent = (int)Mathf.Round(Random.Range(0.0f, 2.0f));

		switch(selectedEvent)
		{
			case 0:
			{
				print("Panne électrique");
				return new PanneElec();
				break;
			}
			case 1:
			{
				print("Dégats des eaux");
				return new DegatsEau();
				break;
			}
			case 2:
			{
				print("Panne d'Internet / Grève des profs");
				return new Internet_Greve();
				break;
			}
			default:
			{
				print("Error : could not select rendom event");
				return new Evenement();
				break;
			}
		}
	}

	public int getEvent()
	{
		return 0;
	}
}
