
using System;
using UnityEngine;

public class Annexe : Building
{
	public int prix = 15000;
	public int charges = 1500;
	public int type = 1;
	public String name = "Annexe d'école";
	public String description = "Avec ce batiment, vous pouvez accueillir plus d'étudiants (s'ils veulent bien venir).";

	public Annexe(int x, int y)
	{
		this.x = x;
		this.y = y;
	}

	public override int getPrix()
	{
		return this.prix;
	}
	
	public override int getCharges()
	{
		return this.charges;
	}

	public override int getType()
	{
		return this.type;
	}
	
	public override String getName()
	{
		return this.name;
	}
	
	public override String getDescription()
	{
		return this.description;
	}
}

