﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using System.Collections;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;


public class PartieEco : MonoBehaviour
{
    public PlacementManager placementManager;
    public InterfaceHeureBanque IH;
	public InterfaceBatiments IB;
    public Etudiants etu; // etudiant = objet de Etudiant
	public int compte;

	private bool finPartie;
	private int[] nbrBat = { 0, 0, 0, 0 };
	private int mois;
	private int semaines;
	private int pret;
	private int alerte;
	private int sommeRembours;
	private int remboursement;

    void Start()
    {
		finPartie = false;

		this.mois = IH.compteurMois;
		this.semaines = IH.semaines;

		compte = 65000;

		pret = 30000;
		alerte = 5000;
		sommeRembours = 0;
		remboursement = 3000;
	}


 	private void CompteQuiAugmente()
    {
		if(IH.compteurMois > this.mois)
		{
			compte += etu.eleve * 50;
			this.mois = IH.compteurMois;
		}
		if(IH.semaines > this.semaines)
		{
			compte += Mathf.RoundToInt(etu.eleve * 3.5f);
			this.semaines = IH.semaines;
		}
    }
    


    private void PrixBatiment()
    {
		// Ajout de batiments
        if (placementManager.buildingsPlaced[3] > nbrBat[3])
        {
            compte -= IB.buildingStat[3].getPrix();
            nbrBat[3] = placementManager.buildingsPlaced[3];
        }

        if (placementManager.buildingsPlaced[2] > nbrBat[2])
        {
			compte -= IB.buildingStat[2].getPrix();
            nbrBat[2] = placementManager.buildingsPlaced[2];
        }

        if (placementManager.buildingsPlaced[1] > nbrBat[1])
        {
			compte -= IB.buildingStat[1].getPrix();
            nbrBat[1] = placementManager.buildingsPlaced[1];
        }

        if (placementManager.buildingsPlaced[0] > nbrBat[0])
        {
			compte -= IB.buildingStat[0].getPrix();
            nbrBat[0] = placementManager.buildingsPlaced[0];
        }

		// Suppression de batiments
		if (placementManager.buildingsPlaced[3] < nbrBat[3])
		{
			compte += (IB.buildingStat[3].getPrix())/2;
			nbrBat[3] = placementManager.buildingsPlaced[3];
		}
		
		if (placementManager.buildingsPlaced[2] < nbrBat[2])
		{
			compte += (IB.buildingStat[2].getPrix())/2;
			nbrBat[2] = placementManager.buildingsPlaced[2];
		}
		
		if (placementManager.buildingsPlaced[1] < nbrBat[1])
		{
			compte += (IB.buildingStat[1].getPrix())/2;
			nbrBat[1] = placementManager.buildingsPlaced[1];
		}
		
		if (placementManager.buildingsPlaced[0] < nbrBat[0])
		{
			compte += (IB.buildingStat[0].getPrix())/2;
			nbrBat[0] = placementManager.buildingsPlaced[0];
		}
    }


   
    private void Charges()
    {
		if(IH.compteurMois > this.mois)
		{
			compte -= (IB.buildingStat[0].getCharges() * nbrBat[0]);
			compte -= (IB.buildingStat[1].getCharges() * nbrBat[1]);
			compte -= (IB.buildingStat[2].getCharges() * nbrBat[2]);
			compte -= (IB.buildingStat[3].getCharges() * nbrBat[3]);
			this.mois = IH.compteurMois;
		}
    }



    private void CompteFinDePartie()
    {
		if(compte <= alerte)
		{
			if(compte < 0 && finPartie)
			{
				print("FIN DE LA PARTIE");
				Application.LoadLevel("MainMenu");
			}
			else
			{
				if(!finPartie)
				{
					finPartie = true;
					compte += pret;
					sommeRembours = 0;
				}
			}
		}

		if(IH.compteurMois > this.mois)
		{
			if(finPartie && sommeRembours <= pret)
				compte -= remboursement;
			if(finPartie && sommeRembours > pret)
				finPartie = false;
			this.mois = IH.compteurMois;
		}
    }

    void OnGUI()
    {
        CompteQuiAugmente();
        PrixBatiment();
       	Charges();
        CompteFinDePartie();
    }
}
