﻿using UnityEngine;
using System.Collections;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

[Serializable]
class PlayerData{

    public int semaines;
    public int compteurMois;
    public int annee;
    public int compte;
    public int reputation;
    public int eleve;
    public int [,] board;

    
}
