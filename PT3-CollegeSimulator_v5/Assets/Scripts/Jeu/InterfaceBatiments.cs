﻿using UnityEngine;
using System.Collections;

public class InterfaceBatiments : MonoBehaviour
{
	public PartieEco eco;
	public GUISkin skinUIBatiments;
	public Texture[] buildingsTiles;
	public Texture[] ouvertImgs;
	public GameObject[] buildingsGO;
	public Building[] buildingStat;
	public int pHorizontal = 30;//Placement horizontal boutons fixer à 30 de base
    public int pVertical= 30; //Placement Vertical boutons adaptés en fonction de la taille verticale de bouton
    public int tHorizontal=70; //Taille Horizontale boutons
    public int tVertical=60; //Taille Verticale boutons
    public int eVertical=10;//Espace entre les boutons verticalement
    public int largeurW;
    public int hauteurW;
    public int posXW;
	public bool render = false;
    public bool ouverture = false;
	public int bug;

	private InterfaceMenu IM;
	private PlacementManager placementManager;
	private Texture menuOuvImg;
	private Building builder;
	private int numBat;
	private int grBatPosH=30;
    private int grBatPosV=10;
    private int grBatLarg=120;
    private int grBatHaut=451;
    private int btOuvPosH=0;
    private int btOuvPosV = Screen.height/2-200;
    private int btOuvLarg = 30;
    private int btOuvHaut = 200;




    void OnGUI()
    {
        IM = GetComponent<InterfaceMenu>();
        largeurW = 150;
        hauteurW = 170;
        GUI.skin = skinUIBatiments;
        
        Rect ouv = new Rect(btOuvPosH, btOuvPosV, btOuvLarg, btOuvHaut);
        Rect groupBat = new Rect(grBatPosH, grBatPosV, grBatLarg, grBatHaut);
      

        if (GUI.Button(ouv, menuOuvImg))
        {
            if (!IM.isNull())
            {
                IM.setZero();
            }
            ouverture = !ouverture;
        }
        if (!((groupBat.Contains(Event.current.mousePosition))||(ouv.Contains(Event.current.mousePosition))) && (Input.GetMouseButtonDown(0) || Input.GetMouseButtonDown(1)))
        {
            ouverture = false;
        }
        if (ouverture == true)
        {
            menuOuvImg = ouvertImgs[1];

            GUI.BeginGroup(groupBat);

            //création d'une box
            GUI.Box(new Rect(0, 0, 120, 450), "Bâtiments");
            int a = pVertical;//la variable a semble nécessaire, sinon un seul bouton est créé et la valeur de pVertical va augmenter de manière infinie.

            Rect curs = new Rect(pHorizontal, a, tHorizontal, tVertical);
                //création des bouttons
                if (GUI.Button(new Rect(pHorizontal, a, tHorizontal, tVertical),new GUIContent(buildingsTiles[0])))
                {
                    numBat = 0;
					if(eco.compte >= buildingStat[0].getPrix ())
                		placementManager.setItems(buildingsGO[numBat], numBat);
					ouverture = false;
                }
                if (curs.Contains(Event.current.mousePosition))
                {

                    render = !render;
                    if (render == true)
                    {
                        
                        //création fenêtre
                        GUI.Window(0, new Rect(pHorizontal+130, a + 10, largeurW, hauteurW), FenetreHover, "Ecole");
                    }
                }

            a = a + tVertical + eVertical;
            curs = new Rect(pHorizontal, a, tHorizontal, tVertical);
			if (GUI.Button(new Rect(pHorizontal, a, tHorizontal, tVertical), new GUIContent(buildingsTiles[1])))
			{
                numBat = 1;
				if(eco.compte >= buildingStat[1].getPrix ())
					placementManager.setItems(buildingsGO[numBat], numBat);
				ouverture = false;
            }

            if (curs.Contains(Event.current.mousePosition))
            {

                render = !render;
                if (render == true)
                {

                    
                    GUI.Window(1, new Rect(pHorizontal + 130, a + 10, largeurW, hauteurW), FenetreHover, "Annexe");

                }
            }

            a = a + tVertical + eVertical;
            curs = new Rect(pHorizontal, a, tHorizontal, tVertical);
			if (GUI.Button(new Rect(pHorizontal, a, tHorizontal, tVertical), new GUIContent(buildingsTiles[2])))
            {
                numBat = 2;
				if(eco.compte >= buildingStat[2].getPrix ())
					placementManager.setItems(buildingsGO[numBat], numBat);
				ouverture = false;
            }

            if (curs.Contains(Event.current.mousePosition))
            {

                render = !render;
                if (render == true)
                {

                    
                    GUI.Window(2, new Rect(pHorizontal + 130, a + 10, largeurW, hauteurW), FenetreHover, "Restaurant");

                }
            }

            a = a + tVertical + eVertical;
            curs = new Rect(pHorizontal, a, tHorizontal, tVertical);
			if (GUI.Button(new Rect(pHorizontal, a, tHorizontal, tVertical), new GUIContent(buildingsTiles[3])))
			{
				numBat = 3;
				if(eco.compte >= buildingStat[3].getPrix ())
					placementManager.setItems(buildingsGO[numBat], numBat);
				ouverture = false;
            }
            if (curs.Contains(Event.current.mousePosition))
            {

                render = !render;
                if (render == true)
                {

                    
                    GUI.Window(3, new Rect(pHorizontal + 130, a + 10, largeurW, hauteurW), FenetreHover, "Résidence");

                }
            }

			a = a + tVertical + eVertical;
			curs = new Rect(pHorizontal, a, tHorizontal, tVertical);
			if (GUI.Button(new Rect(pHorizontal, a, tHorizontal, tVertical), new GUIContent(buildingsTiles[4])))
			{
				numBat = 4;
				placementManager.setItems(buildingsGO[numBat], numBat);
				ouverture = false;
			}
			if (curs.Contains(Event.current.mousePosition))
			{
				
				render = !render;
				if (render == true)
				{
					
					
					GUI.Window(4, new Rect(pHorizontal + 130, a + 10, largeurW, hauteurW), FenetreHover, "Route");
					
				}
			}

			a = a + tVertical + eVertical;
			curs = new Rect(pHorizontal, a, tHorizontal, tVertical);
			if (GUI.Button(new Rect(pHorizontal, a, tHorizontal, tVertical), new GUIContent(buildingsTiles[5])))
			{
				numBat = 5;
				placementManager.setItems(buildingsGO[numBat], numBat);
				ouverture = false;
			}
			if (curs.Contains(Event.current.mousePosition))
			{
				
				render = !render;
				if (render == true)
				{
					
					GUI.Window(0, new Rect(pHorizontal + 130, a + 10, largeurW, hauteurW), FenetreHoverBull, "Bulldozer");
					
				}
			}



         GUI.EndGroup();
        }
        else
			menuOuvImg = ouvertImgs[0];
	}
  

    private void FenetreHover(int id) 
    {
		GUI.Label(new Rect(10, 30, 130, 20), new GUIContent("Prix : " + buildingStat[id].getPrix()));
		GUI.Label(new Rect(10, 60, 130, 20), new GUIContent("Charges : " + buildingStat[id].getCharges()));
		GUI.Label(new Rect(10, 90, 130, 80), new GUIContent(buildingStat[id].getDescription()));
    }

	private void FenetreHoverBull(int id) 
	{
		GUI.Label(new Rect(10, 30, 130, 20), new GUIContent("Prix : " + 0));
		GUI.Label(new Rect(10, 60, 130, 20), new GUIContent("Charges : " + 0));
		GUI.Label(new Rect(10, 90, 130, 80), new GUIContent("Sert à détruire une route ou un batiment. Ne le faites pas avec vos étudiants à l'intérieur !"));
	}

  
    // Use this for initialization
    void Start ()
	{
		placementManager = GetComponent<PlacementManager>();
		menuOuvImg = ouvertImgs[0];

		builder = new Building();
		buildingStat = new Building[5];
		for(int i = 0; i < 5; i++)
			buildingStat[i] = builder.fabriquer(i, -1, -1);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
