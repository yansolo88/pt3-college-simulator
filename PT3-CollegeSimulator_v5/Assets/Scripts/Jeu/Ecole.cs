using System;
using UnityEngine;

public class Ecole : Building
{
	public int prix = 35000;
	public int charges = 4500;
	public int type = 0;
	public String name = "Ecole";
	public String description = "Ici, vous pouvez accueillir des étudiants pour les former à un métier. Enfin, vous pouvez essayer !";

	public Ecole(int x, int y)
	{
		this.x = x;
		this.y = y;
	}

	public override int getPrix()
	{
		return this.prix;
	}

	public override int getCharges()
	{
		return this.charges;
	}

	public override int getType()
	{
		return this.type;
	}

	public override String getName()
	{
		return this.name;
	}

	public override String getDescription()
	{
		return this.description;
	}
}

