﻿using UnityEngine;
using System.Collections;

public class InterfaceBatiments : MonoBehaviour {

	public int pHorizontal = 30;//Placement horizontal boutons fixer à 30 de base
	public int pVertical= 30; //Placement Vertical boutons adaptés en fonction de la taille verticale de bouton
	public int buttonWidth=140; //Taille Horizontale boutons
	public int buttonHeight=60; //Taille Verticale boutons
	public int buttonOffset=10;//Espace entre les boutons verticalement
	public int nombreBoutons=4; //nombre de boutons

	public GUISkin skinUIBatiments;
	public bool render = false;

	public int numBat;
	public int largeurW;
	public int hauteurW;
	public int posXW;
	public bool ouverture = false;
	public Texture[] imgInterface;
	public GameObject[] buildingTiles;

	private InterfaceMenu IM;
	private PlacementManager placementManager;


	void OnGUI()
	{
		IM = GetComponent<InterfaceMenu>();
		largeurW = 150;
		hauteurW = 170;
		GUI.skin = skinUIBatiments;
		Rect cont = new Rect(30, 10, 200, 451);
		//print("" + Input.mousePosition.x + ", " + Input.mousePosition.y);

      
		if (GUI.Button(new Rect(0, 90, 30, 200), "ouverture"))
		{
			if (!IM.isNull())
			{
				IM.setZero();
			}
			ouverture = !ouverture;
		}
		if (!cont.Contains(Input.mousePosition) && 
		    (Input.GetMouseButtonDown(0) || Input.GetMouseButtonDown(1)))
		{
			ouverture = false;
		}
		if (ouverture == true)
		{
            
            
			GUI.BeginGroup(cont);

			//création d'une box
			GUI.Box(new Rect(0, 0, 200, 450), "Menu Bâtiments");
			int a = pVertical;//la variable a semble nécessaire, sinon un seul bouton est créé et la valeur de pVertical va augmenter de manière infinie.
           
			Rect curs = new Rect(pHorizontal+30, Screen.height - a - buttonOffset - buttonHeight, buttonWidth, buttonHeight);
			//Rect curs = new Rect(pHorizontal, pVertical, buttonWidth, buttonHeight);

			//création des bouttons
			if(GUI.Button(new Rect(pHorizontal, a, buttonWidth, buttonHeight),new GUIContent( "   Ecole  ", imgInterface[0])))
			{
				print("Coucou1");
				numBat = 0;
				print("" + numBat);
				placementManager.setItems(buildingTiles[0]);
				ouverture = false;
			}
			if (curs.Contains(Input.mousePosition))
			{
				render = !render;
				if (render == true)
				{

					//création fenêtre
					GUI.Window(0, new Rect(pHorizontal+130, a + 10, largeurW, hauteurW), FenetreHover, "Ecole");
					//print("Coucou1");

				}
			}

			a = a + buttonHeight + buttonOffset;
			//curs = new Rect(pHorizontal + 30, Screen.height - a - buttonOffset - buttonHeight, buttonWidth, buttonHeight);
			if (GUI.Button(new Rect(pHorizontal, a, buttonWidth, buttonHeight), new GUIContent("  Annexe  ", imgInterface[0])))
			{
				numBat = 1;
				print("" + numBat);
				placementManager.setItems(buildingTiles[0]);
				ouverture = false;
			}

			if (curs.Contains(Input.mousePosition))
			{

				render = !render;
				if (render == true)
				{

                    
					GUI.Window(0, new Rect(pHorizontal + 130, a + 10, largeurW, hauteurW), FenetreHover, "Annexe");
					//print("Coucou2");

				}
			}

			a = a + buttonHeight + buttonOffset;
			//curs = new Rect(pHorizontal + 30, Screen.height - a - buttonOffset - buttonHeight, buttonWidth, buttonHeight);
			if (GUI.Button(new Rect(pHorizontal, a, buttonWidth, buttonHeight), new GUIContent(" Resto U  ", imgInterface[0])))
			{
				numBat = 2;
				print("" + numBat);
				placementManager.setItems(buildingTiles[0]);
				ouverture = false;
			}

			if (curs.Contains(Input.mousePosition))
			{

				render = !render;
				if (render == true)
				{
					GUI.Window(0, new Rect(pHorizontal + 130, a + 10, largeurW, hauteurW), FenetreHover, "Restaurant Universitaire");

				}
			}

			a = a + buttonHeight + buttonOffset;
			//curs = new Rect(pHorizontal + 30, Screen.height - a - buttonOffset - buttonHeight, buttonWidth, buttonHeight);
			if (GUI.Button(new Rect(pHorizontal, a, buttonWidth, buttonHeight), new GUIContent("Résidence ", imgInterface[0])))
			{
				numBat = 3;
				print("" + numBat);
				placementManager.setItems(buildingTiles[0]);
				ouverture = false;
			}
			if (curs.Contains(Input.mousePosition))
			{

				render = !render;
				if (render == true)
				{

                    
					GUI.Window(0, new Rect(pHorizontal + 130, a + 10, largeurW, hauteurW), FenetreHover, "Résidence Etudiante");

				}
			}

//			numBat = -1;
//			print("" + numBat);
			GUI.EndGroup();

		}
	}

	private void FenetreHover(int id) 
	{
     
	}
  
	// Use this for initialization
	void Start ()
	{
		placementManager = GetComponent<PlacementManager>();
		numBat = -1;
		print("" + numBat);
	}
	
	// Update is called once per frame
	void Update ()
	{
//		numBat = -1;
//		print("" + numBat);
	}
}
