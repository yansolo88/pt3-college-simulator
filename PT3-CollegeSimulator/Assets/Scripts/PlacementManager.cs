﻿using UnityEngine;
using System.Collections;

public class PlacementManager : MonoBehaviour {
	
	private Transform currentBuilding;
	private bool hasPlaced;
	private GameObject i;

	// Use this for initialization
	void Start ()
	{

	}
	
	// Update is called once per frame
	void Update ()
	{
		Camera camera = GetComponent<Camera>();
		if(currentBuilding != null && !hasPlaced)
		{
			Vector3 m = Input.mousePosition;
			m = new Vector3(m.x, m.y, transform.position.z);
			Vector3 p = camera.ScreenToWorldPoint(m);

			float tx = Mathf.RoundToInt(p.x / 1.3f) * 1.3f;
			float ty = Mathf.RoundToInt(p.y / 0.65f) * 0.65f;

			currentBuilding.position = new Vector3(tx, ty+0.14f, 0);

			if(Input.GetMouseButtonDown(0))
			{
				if(isLegalPosition())
				{
					hasPlaced = true;
					refresh();
				}
			}

			if(Input.GetKey(KeyCode.Escape))
			{
				DestroyObject(i);
			}
		}
	}

	bool isLegalPosition()
	{
		return true;
	}

	public void setItems(GameObject g)
	{
		hasPlaced = false;
		i = Instantiate(g) as GameObject;
		currentBuilding = i.transform;
	}

	void refresh()
	{
		print("refresh done");
	}
}
