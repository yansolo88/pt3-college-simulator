using System;
using UnityEngine;

public class Residence : Building
{
	public int prix = 0;
	public int charges = 0;
	public String name = "Résidence étudiante";
	public String description = "";
	public Texture textureMenu;
	
	public int getPrix()
	{
		return this.prix;
	}
	
	public int getCharges()
	{
		return this.charges;
	}
	
	public String getName()
	{
		return this.name;
	}
	
	public String getDescription()
	{
		return this.description;
	}
	
	public Texture getTexture()
	{
		return textureMenu;
	}
}

