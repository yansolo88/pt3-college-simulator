﻿using UnityEngine;
using System.Collections;

public class InterfaceMenu : MonoBehaviour {
	public int lB = 60; //largeur boutons
	public int hB = 40; //hauteur boutons
	public int nbB = 9;//nombre de boutons
	public int pBo = 19;//placement du groupe des boutons par la droite
	public int eB = 2;//espacement des boutons (optionnel suivant l'esthétique)
	public int numBm;//numéro boutton menu
	private bool[] render=new bool[9];//tableau de booléen pour savoir quelle fenêtre est ouverte
	public GUISkin skinUIBarreAction;// skin de la barre menu
	public Texture img;
	private InterfaceBatiments IB;
	private Rect cont;

	private EventManager em = new EventManager();
	private Building selectedBuilding;
	private Event selectedEvent;

  
	public void OnGUI()
	{
		IB = GetComponent<InterfaceBatiments>();
		GUI.skin = skinUIBarreAction;//mise en place du skin de la barre de menu

		GUI.Box(new Rect(Screen.width / 2 - 300, Screen.height - 45, 600, hB), "");
		GUI.BeginGroup(new Rect(Screen.width / 2 - 300, Screen.height - 45, 600, hB));

		int a = pBo;// l'initialisation d'une variable a semble obligatoire dans l'exécution à suivre
       
		for (int i = 0; i < nbB; i++)//création des boutons de la barre de menu
		{
            
			//ici le nom des bouttons sont "B.M"+i, sachant que les boutons seront certainement des images (une simple modification du script
			//le fera),il suffira simplement de créer un tableau d'images prenant en compte la variable i.

			if (GUI.Button(new Rect(a, 0, lB, hB), new GUIContent("B.M" + i, img)))// si le boutton est cliqué met à jour le tableau d'ouverture des fenêtres
			{
				numBm = i;
				render[numBm] = !render[numBm];

				if (IB.ouverture == true)
				{
					IB.ouverture = false;
				}

			}
         
            
			a = a + lB + eB;

		}
		GUI.EndGroup();
		if (Input.GetKey(KeyCode.Escape))
		{
			setZero();
			IB.ouverture = false;

		}

     
		if (numBm == 0 || numBm==1)
		{
			cont = new Rect(Screen.width / 2 - 100, 240, 200, 300);
			if (((!cont.Contains(Input.mousePosition))) && (Input.GetMouseButtonDown(0) || Input.GetMouseButtonDown(1)))
			{
				setZero();
			}
		}
		else
		{
			cont = new Rect(Screen.width / 2 - 250, 240, 500, 300);
			if (((!cont.Contains(Input.mousePosition))) && (Input.GetMouseButtonDown(0) || Input.GetMouseButtonDown(1)))
			{
				setZero();
			}
		}
		actionBouton(numBm);//lors du clique d'un boutton, va exécuter l'action défini dans actionBouton.La fonction ne s'intègre apparament pas dans le if ci-dessus.
       
	}

	//La fonction actionBouton pourrait éventuellement être optimisée afin de gérer le numéro de case des render
	//automatiquement tout comme les paramètres des fonctions réinitialisationRender

	private void actionBouton(int a)//définition des actions des boutons
	{

		switch (a)
		{
			case 0:

				if (render[0] == true)
				{
					réinitialisationRender(0);//voir fonction réinitialisationRender
					//création fenêtre
					GUI.Window(0, new Rect(Screen.width / 2 - 100, 80, 200, 300), MenuPause, "Menu Pause");
					Time.timeScale = 0;//pause
				}
				else
				{
					Time.timeScale = 1;//reprise
				}
				break;
			case 1:
                
				if (render[1] == true)
				{
				réinitialisationRender(1);
				GUI.Window(1, new Rect(Screen.width / 2 - 100, 80, 200, 300), MenuTest, "Menu test");
				}
                break;
            case 2:
                
                if (render[2] == true)
                {
					réinitialisationRender(2);
                    GUI.Window(2, new Rect(Screen.width / 2 - 250, 80, 500, 300), MenuTest, "Menu test");
                }
				break;
			case 3:
                
				if (render[3] == true)
				{
                    réinitialisationRender(3);
                    GUI.Window(3, new Rect(Screen.width / 2 - 250, 80, 500, 300), MenuTest, "Menu test");
				}
                break;
            case 4:
                
                if (render[4] == true)
                {
                    réinitialisationRender(4);
                    GUI.Window(4, new Rect(Screen.width / 2 - 250, 80, 500, 300), MenuTest, "Menu test");
                }
                break;
            case 5:
                
                if (render[5] == true)
                {
                    réinitialisationRender(5);
                    GUI.Window(5, new Rect(Screen.width / 2 - 250, 80, 500, 300), MenuTest, "Menu test");
                }
                break;
            case 6:
                
                if (render[6] == true)
                {
                    réinitialisationRender(6);
                    GUI.Window(6, new Rect(Screen.width / 2 - 250, 80, 500, 300), MenuTest, "Menu test");
                }
                break;
            case 7:
                
                if (render[7] == true)
                {
                    réinitialisationRender(7);
                    GUI.Window(7, new Rect(Screen.width / 2 - 250, 80, 500, 300), MenuTest, "Menu test");
                }
                break;
            case 8:
                
                if (render[8] == true)
                {
                    réinitialisationRender(8);
                    selectedBuilding = em.selectBuilding();
                }
                break;
        }
    }


    private void réinitialisationRender(int a)//lorsque qu'une fenêtre est ouverte, ferme toutes les autres pour une meilleur naviguation
    {
        for (int i = 0; i < nbB; i++)
        {
            render[i] = false;
        }
        render[a] = true;
    }

    public void setZero()
    {
        for (int i = 0; i < nbB; i++)
        {
            render[i] = false;
        }
    }

    public bool isNull()
    {
        for(int i=0; i < render.Length; i++)
        {
            if (render[i] != false)
            {
                return false;
            }
        }
        return true;
    }


    private void MenuPause(int id) //contenu de la fenêtre MenuPause
    {
        if (GUI.Button(new Rect(10, 30, 180, 30), "Save"))
        {

        }
        if(GUI.Button(new Rect(10, 70, 180, 30), "Exit"))
        {
            //Application.Quit();
        }
        if (GUI.Button(new Rect(10, 110, 180, 30), "Options"))
        {

        }
    }


    private void MenuTest(int id)//contenu de la fenêtre MenuTest
    {
      
    }

    // Use this for initialization
    void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {
        
      

    }
}
