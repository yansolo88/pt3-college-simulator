﻿using UnityEngine;
using System;
using System.Collections.Generic;
using Random = UnityEngine.Random;
using System.Linq;

public class BoardManager : MonoBehaviour
{
	public int columns;
	public int rows;
	public GameObject[] groundTiles;	// tuiles de terrain
	public GameObject[,] board;			// terrain
	public Transform boardHolder;
	//private List <Vector3> gridPositions = new List<Vector3>();

	public void refresh()
	{
		List<GameObject> listGame = new List<GameObject>();

		for(int i=0 ; i<columns ; i++)
		{
			for(int j=0 ; j<rows ; j++)
			{
				listGame.Add(board[i,j]);
			}
		}

		listGame = listGame.OrderByDescending(x => x.transform.position.y).ToList();

		for(int i=0 ; i<listGame.Count ; i++)
		{
			listGame[i].GetComponent<SpriteRenderer>().sortingOrder = i;
		}

		print("refresh done");
	}

	void initialiseBoard()
	{
		board = new GameObject[columns, rows];
	}	

	void boardSetup()
	{
		/*
		boardHolder = new GameObject("Board").transform;

		GameObject toInstantiate = groundTiles[0];

		for(int x = 0; x < columns + 1; x++)
		{			
			for(int y = 0; y < rows + 1; y++)
			{
				GameObject instance = Instantiate (toInstantiate, 
				                                   new Vector3 (1.3f*x, (y - 0.35f*y), 0f), 
				                                   Quaternion.identity) as GameObject;

				instance.transform.SetParent (boardHolder);

				GameObject instance2 = Instantiate (toInstantiate, 
				                                    new Vector3 ((0.65f + 1.3f*x), (0.325f + 0.65f*y), 0f), 
				                                    Quaternion.identity) as GameObject;

				instance2.transform.SetParent (boardHolder);
			}
		}
		*/

		initialiseBoard();
		boardHolder = new GameObject("Board").transform;
		GameObject toInstantiate = groundTiles[0];

		for (int i = 0; i < columns; i++)
		{
			for (int j = 0; j < rows; j++)
			{
				//print("" + i + ", " + j);

				float x = 0;
				float y = 0;

				if(j%2 == 0)
				{
					x = 1.3f * i;
					y = (j - 0.35f * j) - 0.325f * j;
				}
				else
				{
					x = (1.3f * i) + 0.65f;
					y = (0.65f * j - 0.325f * j);
				}

				GameObject instance = Instantiate (toInstantiate, 
				                                   new Vector3 (x, y, 0f), 
				                                   Quaternion.identity) as GameObject;

				instance.transform.SetParent (boardHolder);
				board[i,j] = instance;

			}
		}
	}

	public void SetupScene()
	{
		boardSetup();
		refresh();
	}
	 
}
