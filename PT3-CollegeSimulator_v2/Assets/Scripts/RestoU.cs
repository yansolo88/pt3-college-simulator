using System;
using UnityEngine;

public class RestoU : Building
{
	public int prix = 0;
	public int charges = 0;
	public int type = 2;
	public String name = "Resto Universitaire";
	public String description = "";
	public Texture textureMenu;

	public override int getPrix()
	{
		return this.prix;
	}
	
	public override int getCharges()
	{
		return this.charges;
	}

	public override int getType()
	{
		return this.type;
	}
	
	public override String getName()
	{
		return this.name;
	}
	
	public override String getDescription()
	{
		return this.description;
	}
	
	public override Texture getTexture()
	{
		return textureMenu;
	}
}

