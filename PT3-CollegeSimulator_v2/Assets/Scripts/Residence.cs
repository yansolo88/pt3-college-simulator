using System;
using UnityEngine;

public class Residence : MonoBehaviour
{
	public int prix = 0;
	public int charges = 0;
	public int type = 3;
	public String name = "Résidence étudiante";
	public String description = "Ceci est une description";
	public Texture textureMenu;

	public int getPrix()
	{
		return this.prix;
	}
	
	public int getCharges()
	{
		return this.charges;
	}

	public int getType()
	{
		return this.type;
	}
	
	public String getName()
	{
		return this.name;
	}

	public String getDescription()
	{
		return this.description;
	}
	
	public Texture getTexture()
	{
		return textureMenu;
	}
}

