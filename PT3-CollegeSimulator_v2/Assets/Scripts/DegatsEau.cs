﻿using UnityEngine;
using System.Collections;

public class DegatsEau : Event 
{
	private int coutReparations = 0;
	private int malus = 0;

	public override int getCout()
	{
		return coutReparations;
	}

	public override int getMalus()
	{
		return malus;
	}
}
