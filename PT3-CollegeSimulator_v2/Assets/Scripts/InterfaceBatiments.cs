﻿using UnityEngine;
using System.Collections;

public class InterfaceBatiments : MonoBehaviour {

	public int pHorizontal = 30;//Placement horizontal boutons fixer à 30 de base
	public int pVertical= 30; //Placement Vertical boutons adaptés en fonction de la taille verticale de bouton
	public int tHorizontal=140; //Taille Horizontale boutons
	public int tVertical=60; //Taille Verticale boutons
	public int eVertical=10;//Espace entre les boutons verticalement
	public int nombreBoutons=6; //nombre de boutons
	public GUISkin skinUIBatiments;
	public bool render = false;
	public int numBat;
	public int largeurW;
	public int hauteurW;
	public int posXW;
	public bool ouverture = false;
	public Texture[] imgInterface;
	public GameObject[] buildingTiles;

	private InterfaceMenu IM;
	private PlacementManager placementManager;




	void OnGUI()
	{
		IM = GetComponent<InterfaceMenu>();
		largeurW = 150;
		hauteurW = 170;
		GUI.skin = skinUIBatiments;
		Rect cont = new Rect(30, 10, 200, 451);

      
		if (GUI.Button(new Rect(0, 90, 30, 200), "ouverture"))
		{
			if (!IM.isNull())
			{
				IM.setZero();
			}
			ouverture = !ouverture;
		}
		if (((!cont.Contains(Input.mousePosition))) && (Input.GetMouseButtonDown(0) || Input.GetMouseButtonDown(1)))
		{
			ouverture = false;
		}
		if (ouverture == true)
		{
            
            

			GUI.BeginGroup(cont);

			//création d'une box
			GUI.Box(new Rect(0, 0, 200, 450), "Menu Bâtiments");
			int a = pVertical;//la variable a semble nécessaire, sinon un seul bouton est créé et la valeur de pVertical va augmenter de manière infinie.
           
			Rect curs = new Rect(pHorizontal+30, Screen.height - a - eVertical - tVertical, tHorizontal, tVertical);

			//création des bouttons
			if (GUI.Button(new Rect(pHorizontal, a, tHorizontal, tVertical),new GUIContent( "   Ecole  ", imgInterface[0])))
			{
				numBat = 0;
				placementManager.setItems(buildingTiles[0], 0);
				ouverture = false;
				// actionBouton(numBm);
                    
			}
			if (curs.Contains(Input.mousePosition))
			{

				render = !render;
				if (render == true)
				{

					//création fenêtre
					GUI.Window(0, new Rect(pHorizontal+130, a + 10, largeurW, hauteurW), FenetreHover, "Menu Pause");

				}
			}

			a = a + tVertical + eVertical;
			curs = new Rect(pHorizontal + 30, Screen.height - a - eVertical - tVertical, tHorizontal, tVertical);
			if (GUI.Button(new Rect(pHorizontal, a, tHorizontal, tVertical), new GUIContent("  Annexe  ", imgInterface[1])))
			{
				numBat = 1;
				placementManager.setItems(buildingTiles[1], 1);
				ouverture = false;
			}

			if (curs.Contains(Input.mousePosition))
			{

				render = !render;
				if (render == true)
				{

                    
					GUI.Window(0, new Rect(pHorizontal + 130, a + 10, largeurW, hauteurW), FenetreHover, "Menu Pause");

				}
			}

			a = a + tVertical + eVertical;
			curs = new Rect(pHorizontal + 30, Screen.height - a - eVertical - tVertical, tHorizontal, tVertical);
			if (GUI.Button(new Rect(pHorizontal, a, tHorizontal, tVertical), new GUIContent(" Resto U  ", imgInterface[2])))
			{
				numBat = 2;
				placementManager.setItems(buildingTiles[2], 2);
				ouverture = false;
			}

			if (curs.Contains(Input.mousePosition))
			{

				render = !render;
				if (render == true)
				{

                    
					GUI.Window(0, new Rect(pHorizontal + 130, a + 10, largeurW, hauteurW), FenetreHover, "Menu Pause");

				}
			}

			a = a + tVertical + eVertical;
			curs = new Rect(pHorizontal + 30, Screen.height - a - eVertical - tVertical, tHorizontal, tVertical);
			if (GUI.Button(new Rect(pHorizontal, a, tHorizontal, tVertical), new GUIContent("Résidence ", imgInterface[3])))
			{
				numBat = 3;
				placementManager.setItems(buildingTiles[3], 3);
				ouverture = false;
			}
			if (curs.Contains(Input.mousePosition))
			{

				render = !render;
				if (render == true)
				{

                    
					GUI.Window(0, new Rect(pHorizontal + 130, a + 10, largeurW, hauteurW), FenetreHover, "Menu Pause");

				}
			}

			a = a + tVertical + eVertical;
			curs = new Rect(pHorizontal + 30, Screen.height - a - eVertical - tVertical, tHorizontal, tVertical);
			if (GUI.Button(new Rect(pHorizontal, a, tHorizontal, tVertical), new GUIContent("Routes ", imgInterface[4])))
			{
				numBat = -1;
				placementManager.setItems(buildingTiles[4], 4);
				ouverture = false;
			}
			if (curs.Contains(Input.mousePosition))
			{
				
				render = !render;
				if (render == true)
				{
					
					
					GUI.Window(0, new Rect(pHorizontal + 130, a + 10, largeurW, hauteurW), FenetreHover, "Menu Pause");
					
				}
			}

			a = a + tVertical + eVertical;
			curs = new Rect(pHorizontal + 30, Screen.height - a - eVertical - tVertical, tHorizontal, tVertical);
			if (GUI.Button(new Rect(pHorizontal, a, tHorizontal, tVertical), new GUIContent("Bulldozer ", imgInterface[5])))
			{
				numBat = -1;
				placementManager.setItems(buildingTiles[5], -1);
				ouverture = false;
			}
			if (curs.Contains(Input.mousePosition))
			{
				
				render = !render;
				if (render == true)
				{
					
					
					GUI.Window(0, new Rect(pHorizontal + 130, a + 10, largeurW, hauteurW), FenetreHover, "Menu Pause");
					
				}
			}


			GUI.EndGroup();

		}
	}
    

   


    


	private void FenetreHover(int id) 
	{
     
	}


  
	// Use this for initialization
	void Start ()
	{
		placementManager = GetComponent<PlacementManager>();
	}
	
	// Update is called once per frame
	void Update ()
	{
	
	}
}
