﻿using UnityEngine;
using System.Collections;

public class CameraManager : MonoBehaviour {

	public float CameraMoveSpeed;
	public int zoneMorte;

	// Gestion du zoom par la molette de la souris
	void ZoomScroll()
	{
		var varScroll = Input.GetAxis("Mouse ScrollWheel");
		if(varScroll < 0f)
		{
			if(Camera.main.orthographicSize < 10)
			{
				Camera.main.orthographicSize += 0.5f;
			}
		}
		if(varScroll > 0f)
		{
			if(Camera.main.orthographicSize > 4)
			{
				Camera.main.orthographicSize -= 0.5f;
			}
		}
	}

	// Gestion du déplacement de la caméra par les touches directionnelles
	void CameraMove()
	{
		if(Input.GetKey(KeyCode.UpArrow) || Input.mousePosition.y > Camera.main.pixelHeight - zoneMorte)
		{
			transform.Translate(new Vector3(0,CameraMoveSpeed * Time.deltaTime,0));
		}
		if(Input.GetKey(KeyCode.DownArrow) || Input.mousePosition.y < zoneMorte)
		{
			transform.Translate(new Vector3(0,-CameraMoveSpeed * Time.deltaTime,0));
		}
		if(Input.GetKey(KeyCode.RightArrow) || Input.mousePosition.x > Camera.main.pixelWidth - zoneMorte)
		{
			transform.Translate(new Vector3(CameraMoveSpeed * Time.deltaTime,0,0));
		}
		if(Input.GetKey(KeyCode.LeftArrow) || Input.mousePosition.x < zoneMorte)
		{
			transform.Translate(new Vector3(-CameraMoveSpeed * Time.deltaTime,0,0));
		}
	}

	// Update is called once per frame
	void Update ()
	{
		ZoomScroll();
		CameraMove();
	}
}
