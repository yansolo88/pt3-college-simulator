﻿using UnityEngine;
using System.Collections;

public class InterfaceBatiments : MonoBehaviour {

    public int pHorizontal = 30;//Placement horizontal boutons fixer à 30 de base
    public int pVertical= 30; //Placement Vertical boutons adaptés en fonction de la taille verticale de bouton
    public int tHorizontal=70; //Taille Horizontale boutons
    public int tVertical=60; //Taille Verticale boutons
    public int eVertical=10;//Espace entre les boutons verticalement
    public int nombreBoutons=4; //nombre de boutons
    public GUISkin skinUIBatiments;

    void OnGUI()
    {
        GUI.skin = skinUIBatiments;
        GUI.BeginGroup(new Rect(10, 10, 800, 475));
        //création d'une box
        GUI.Box(new Rect(0, 0, 120, 450), "Menu Bâtiments");
        int a = pVertical;//la variable a semble nécessaire, sinon un seul bouton est créé et la valeur de pVertical va augmenter de manière infinie.
        for (int i = 1; i <= nombreBoutons; i++)
        {
            
            //création des bouttons
            if (GUI.Button(new Rect(pHorizontal, a, tHorizontal, tVertical), "Bâtiment"+i))
            {

            }

            a = a + tVertical + eVertical;

            
        }
        GUI.EndGroup();
    }


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
